const { createApp } = Vue;

createApp({
    data() {
        return {
            username: '',
            password: '',
            error: null,
            correto: null,

            //arrays para armazenamento dos usuários e senhas
            usuarios: ['admin', 'viniBolota'],
            senhas: ['1234', '1234'],

            //variáveis para armazenamento do usuário e senha que será cadrastado
            newUsername: '',
            newPassword: '',
            confirmPassword: '',

            //Interação das mensagens de texto
            mostrarEntrada: false,

            mostrarLista: false,
        }
    },

    methods: {
        login() {
            setTimeout(() => {
                if ((this.username === 'admin' && this.password === '1234') || (this.username === 'viniBolota' && this.password === '1234')) {
                    this.correto = "Login Realializado com Sucesso!";
                    this.error = null;
                }

                else {
                    this.error = "Nome ou senha incorretos!";
                    this.correto = null;
                }
            }, 1000);
        },//fechamento login

        loginArray(){
            if(localStorage.getItem('usuarios') && localStorage.getItem('senhas')){
                this.usuarios = JSON.parse(localStorage.getItem('usuarios'));
                this.senhas = JSON.parse(localStorage.getItem('senhas'));
            }
            
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                //uso de uma constante para armazenar o número do index onde o usuário "procurado" está "guardado" no array
                const index = this.usuarios.indexOf(this.username);
                if (index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.correto = "Login efetuado com sucesso!"

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);
                }

                else {
                    this.correto = null;
                    this.error = "Usuário ou senha incorretos!";
                }

                this.username = '';
                this.password = '';
            }, 500);
        },

        adicionarUsuario() {
            this.mostrarEntrada = false;

            //update no conteúdo dos arrays para recuperar os dados armazenados no local storage
            if(localStorage.getItem('usuarios') && localStorage.getItem('senhas')){
                this.usuarios = JSON.parse(localStorage.getItem('usuarios'));
                this.senhas = JSON.parse(localStorage.getItem('senhas'));
            }

            setTimeout(() => {
                this.mostrarEntrada = true;
                if (!this.usuarios.includes(this.newUsername) && this.newUsername !== '') {
                    if (this.newPassword && this.newPassword === this.confirmPassword) {
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassword);

                        //atualizando os valores dos arrays no armazenamento local
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));

                        this.newUsername = '';
                        this.newPassword = '';
                        this.confirmPassword = '';

                        this.correto = 'Usuário cadastrado com sucesso!';
                        this.error = null;
                    }
                    else {
                        this.error = 'Por favor, digite uma senha válida!';
                    }
                }

                else {
                    this.error = 'Por favor, informe um usuário válido!';
                    this.correto = null;
                }
            }, 500);

        },//fechamento adicionarUsuario

        verCadastrados(){
            if(localStorage.getItem('usuarios') && localStorage.getItem('senhas')){
                this.usuarios = JSON.parse(localStorage.getItem('usuarios'));
                this.senhas = JSON.parse(localStorage.getItem('senhas'));
            }
            this.mostrarLista = !this.mostrarLista;
        },

        paginaCadastro(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                this.correto = 'Carregando página de cadastro!';
            }, 1000);

            setTimeout(() => {
                window.location.href = 'cadastro.html';
            }, 1000)
        },
    },

}).mount("#app"); //fechamento createApp
