const{createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/Senai_logo.png',
                './imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg?q=10&h=200',
                'https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Palmeiras_logo.svg/1200px-Palmeiras_logo.svg.png',
                'https://static-wp-tor15-prd.torcedores.com/wp-content/uploads/2019/10/spurs.png'
            ]
        };//fim do return
    },//fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex]
        },//fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet]
        }//fim randomImageInternet
    },//fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length);
        }
    },//fim methods
}).mount("#app");
