const {createApp} = Vue;
createApp({
    data(){
        return{
            testeSpan: false,
            isLampadaLigada: false,
        }//fim return
    },//fim data

    methods:{
        handleTest: function()
        {
            this.testeSpan = !this.testeSpan;
        },//fim handleTest

        toggleLampada: function()
        {
            this.isLampadaLigada = !this.isLampadaLigada;
        }
    },//fi methods

}).mount("#app")