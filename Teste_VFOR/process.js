const {createApp} = Vue;

createApp({
    data(){
        return{
            show: false,
            itens: ["Bola", "Bolsa", "Tenis"],
        };//fechamento return
    },//fechamento data

    methods:{
        showItens:function(){
            this.show = !this.show;
        },//fechamento showItens
    },//fechamento methods

}).mount("#app");//fechamento app
